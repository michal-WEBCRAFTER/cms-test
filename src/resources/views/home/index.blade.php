@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-auto mr-auto">
                @if(count($posts) > 0)

                    <table class="table table-hover table-dark">
                        <thead>
                        <tr>
                            <th scope="col">Lp:</th>
                            <th scope="col">Tytuł</th>
                            <th scope="col">Post</th>
                            <th scope="col">Napisano</th>
                            <th scope="col">Edytowano</th>
                            <th scope="col">Szczegóły</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $lp =1;
                        @endphp
                        @foreach($posts as $allposts)
                            <tr>
                                <th scope="row">{{$lp++}}</th>
                                <td>{{$allposts->title}}</td>
                                <td>{!! $allposts->body !!}</td>
                                <td>{{$allposts->created_at}}</td>
                                <td>{{$allposts->updated_at}}</td>
                                <td>
                                    <a class="btn btn-small btn-success" href="{{ URL::to('posts/' . $allposts->id) }}">Zobacz</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <p>Nie ma postów</p>
                @endif
            </div>
        </div>
    </div>

@endsection