@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('pages.addpost')
            <div class="col-md-10 col-auto mr-auto">
                @if(count($posts) > 0)
                    <table class="table table-hover table-dark">
                        <thead>
                        <tr>
                            <th scope="col">Lp:</th>
                            <th scope="col">Tytuł</th>
                            <th scope="col">Post</th>
                            <th scope="col">Napisano</th>
                            <th scope="col">Edytowano</th>
                            <th scope="col">Szczegóły</th>
                            <th scope="col">Edycja</th>
                            <th scope="col">Pozbądż się tego posta</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $lp =1;
                        @endphp
                        @foreach($posts as $allposts)
                            <tr>
                                <th scope="row">{{$lp++}}</th>
                                <td>{{$allposts->title}}</td>
                                <td>{!! $allposts->body !!}</td>
                                <td>{{$allposts->created_at}}</td>
                                <td>{{$allposts->updated_at}}</td>
                                <td>
                                    <a class="btn btn-small btn-success" href="{{ URL::to('posts/' . $allposts->id) }}">Zobacz</a>
                                </td>
                                <td>
                                        <a class="btn btn-small btn-info"
                                           href="{{ URL::to('posts/' . $allposts->id. '/edit') }}">Edytuj</a>
                                </td>
                                <td>
                                        {{ Form::open(array('url' => 'posts/' . $allposts->id, 'class' => 'pull-left')) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                        {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <p>Nie ma postów</p>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>
        img {
            width: 100px;
            height: 100px;
        }
    </style>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop