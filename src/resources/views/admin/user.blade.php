@if( Auth::user()->admin == 0)
    @extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Zarządzanie użytkownikami</h1>
@stop

@section('content')
<div class="jumbotron">
  <h1 class="display-4">Dodaj użytkownika!</h1>
  <a class="btn btn-primary btn-lg" href="/admin/user/adduser" role="button">Nowy użytkownik</a>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
@endif