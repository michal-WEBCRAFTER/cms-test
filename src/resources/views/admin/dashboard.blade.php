@if( Auth::user()->admin == 0)

    @extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')

        <h2>Lists users.</h2>

    @if(count($users) >1)
        <table id="table" class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Imię i nazwisko</th>
                <th scope="col">E-mail</th>
                <th scope="col">Kiedy dołączył</th>
                <th scope="col">Kiedy była edycja</th>
                <th scope="col">Szczegóły</th>
                <th scope="col">Modyfikacja</th>
                <th scope="col">Usuń</th>
            </tr>
            </thead>
            <tbody id="tablecontents">
            @php
                $i =1;
            @endphp
            @foreach($users as $listsUsers)
                <tr class="row1" data-id="{{ $listsUsers->id }}">
                    <th scope="row">{{$i++}}</th>
                    <td>{{$listsUsers->name}}</td>
                    <td>{{$listsUsers->email}}</td>
                    <td>{{$listsUsers->created_at}}</td>
                    <td>{{$listsUsers->updated_at}}</td>
                    <td>
                        <a class="btn btn-small btn-success" href="{{ URL::to('admin/' . $listsUsers->id) }}">Zobacz</a>
                    </td>

                    <td>
                        <a class="btn btn-small btn-info" href="{{ URL::to('admin/' . $listsUsers->id. '/edit') }}">Edytuj</a>
                    </td>
                    <td valign="center" align="center">
                        {{ Form::open(array('url' => 'admin/' . $listsUsers->id, 'class' => 'pull-left')) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>Nie ma użytkowników</p>
    @endif

@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css"/>
@stop

@section('js')
    <script src="{{ asset('js/ajaxcrud.js') }}"></script>
@stop
@endif
