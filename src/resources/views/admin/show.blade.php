@if( Auth::user()->admin == 0)
    @extends('adminlte::page')

@section('title', 'Dashboard')
@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
@section('content_header')
    <h1>Zobacz użytkownika</h1>
@stop

@section('content')
    <div class="container">

        <h1>Showing {{ $user->name }}</h1>

        <div class="jumbotron text-center">
            <p>
                <strong>Email:</strong> {{ $user->email }}<br>
                <strong>Zarejestrowany:</strong> {{ $user->created_at }}<br>
                <strong>Edytowany:</strong> {{ $user->updated_at }}<br>
                <strong>Tyle razy zmienał swje hasło:</strong> {{ $user->password_changed }}
            </p>
        </div>
    </div>

@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
@endif