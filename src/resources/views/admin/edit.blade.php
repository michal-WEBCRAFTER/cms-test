@if( Auth::user()->admin == 0)
    @extends('adminlte::page')

    @section('title', 'Dashboard')
@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
@section('content_header')
    <h1>Edytuj użytkownika</h1>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edycja {{ $user->name }}</div>

                    <!-- if there are creation errors, they will show here -->
                    {{ Html::ul($errors->all()) }}
                    <div class="panel-body">

                        {{ Form::model($user, array('route' => array('admin.update', $user->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
                        {{ csrf_field() }}

                        <div class="form-group">
                            {{ Form::label('name', 'Name', array('class' => 'col-md-4 control-label')) }}
                            <div class="col-md-6">
                                {{ Form::text('name', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('email', 'Email', array('class' => 'col-md-4 control-label')) }}
                            <div class="col-md-6">
                                {{ Form::email('email', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('password', 'Password', array('class' => 'col-md-4 control-label')) }}
                            <div class="col-md-6">
                                {{ Form::password('password', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('password_confirmation', 'Password confirmation', array('class' => 'col-md-4 control-label')) }}
                            <div class="col-md-6">
                                {{ Form::password('password_confirmation', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('admin', 'Admin', array('class' => 'col-md-4 control-label')) }}
                            <div class="col-md-6">
                                {{ Form::checkbox('admin',1, array('id'=>'admin')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                {{ Form::submit('Edytuj użytkownika', array('class' => 'btn btn-primary')) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
@endif