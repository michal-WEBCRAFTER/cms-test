
    <div class="panel panel-default">
        <div class="panel-heading">Dodaj post</div>
        <div class="panel-body">

            {{ Form::model(array('method' => 'POST', 'class' => 'form-horizontal')) }}
            {{ csrf_field() }}

            <div class="form-group">
                {{ Form::label('title', 'Tytuł', array('class' => 'col-md-4 control-label')) }}
                <div class="col-md-12">
                    {{ Form::text('title', null, array('class' => 'form-control')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('body', 'Post', array('class' => 'col-md-4 control-label')) }}
                <div class="col-md-12">
                    {{ Form::textarea('body', null, array('class' => 'form-control','size' => '30x5')) }}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    {{ Form::submit('Dodaj post', array('class' => 'pull-right btn btn-primary')) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
