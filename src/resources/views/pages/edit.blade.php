@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edycja {{ $post->title }}</div>

                    <!-- if there are creation errors, they will show here -->
                    {{ Html::ul($errors->all()) }}
                    <div class="panel-body">

                        {{ Form::model($post, array('route' => array('posts.update', $post->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
                        {{ csrf_field() }}

                        <div class="form-group">
                            {{ Form::label('title', 'Tytuł', array('class' => 'col-md-4 control-label')) }}
                            <div class="col-md-12">
                                {{ Form::text('title', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('body', 'Post', array('class' => 'col-md-4 control-label')) }}
                            <div class="col-md-12">
                                {{ Form::textarea('body', null, array('class' => 'form-control','size' => '30x5')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                {{ Form::submit('Edytuj post', array('class' => 'btn btn-primary')) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')

@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop