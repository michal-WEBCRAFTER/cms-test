@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Tytuł postu: "{{ $post->title }}"</h1>
            <div class="jumbotron text-center">
                <p>
                    <strong>Opis:</strong> {!! $post->body !!}<br>
                    <strong>Dodany:</strong> {{ $post->created_at }}<br>
                    <strong>Edytowany:</strong> {{ $post->updated_at }}<br>
                </p>
            </div>
        </div>
        <div class="row">
            <div id="comment-form">
                <div class="panel-body">

                    {{ Form::model(array('route' => 'posts/'.$post->id,'method' => 'POST', 'class' => 'form-horizontal')) }}
                    {{ csrf_field() }}

                    <div class="form-group">
                        {{ Form::label('comment', 'Post', array('class' => 'col-md-4 control-label')) }}
                        <div class="col-md-12">
                            {{ Form::textarea('comment', null, array('class' => 'form-control','size' => '5x5')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            {{ Form::submit('Dodaj Komentarz', array('class' => 'pull-right btn btn-primary')) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @if(!isset($_POST['form']))
                <div class="p-3 mb-2 bg-success"><p class="text-center text-white">{{ Session::get('message') }}</p>
                </div>
            @endif
            <h3><strong>Comments: {{$post->comments->count()}}</strong></h3>
            @foreach($post->comments as $comments)

                    <div class="col-3 comment" width="200">
                    <p><strong>Comment: </strong>
                        <img src="" alt="author" class="author-img">
                        <br>
                    <div class="author-info">
                        <strong>{{ $comments->user->name}}</strong>
                        : date added {{$comments->created_at}}
                    </div>
                    </p>
                    <div class="content"><p>{!! $comments->comment !!}</p></div>
                        @if($comments->user->name == Auth::user()->name)
                        <div class="content">
                            <a href="{{route('delete.comm', ['id' => $comments->id_comment ])}}">Usuń</a>
                        </div>
                    @endif
                </div>
                @endforeach
            {!! $post->comments->render() !!}.

        </div>
    </div>
@endsection

@section('css')

@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop