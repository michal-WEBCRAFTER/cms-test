@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>Remove this comment</h1>
                <p>
                    <strong>Comment: </strong> {{$commentes->comment}} <br>
                </p>
                {{ Form::open(array('route' => ['pages.delete' ,$commentes->id_comment], 'method' => 'delete')) }}
                {{ Form::submit('Remove this comment', array('class' => 'deleteComment btn btn-danger')) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection

@section('css')

@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop