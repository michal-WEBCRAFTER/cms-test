<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/', 'HomeController@index');
Auth::routes();
Route::post('/login', [
    'uses' => 'HomeController@homeLogin',
    'as' => 'login',
]);

Route::get('/logout', [
    'uses' => 'HomeController@logout',
    'as' => 'logout',
]);

Route::group(['middleware' => 'auth'], function () {
//    //users
    Route::resource('admin', 'AdminController');
//    //posts
    Route::resource('posts', 'PostsController');
//    //comments
    Route::resource('comments', 'CommentController');
    //get
    Route::get('/home', 'HomeController@home')->name('home');
    Route::get('/posts', 'PostsController@index')->name('posts');
    Route::get('/admin', 'AdminController@index')->name('admin.dashboard');
    Route::get('/posts/{id}/del', 'CommentController@delete')->name('delete.comm');
    Route::get('/admin/user/adduser', 'ProfileController@index')->name('adduser');
    Route::get('/auth/login-first-time', 'Auth/LoginController@firstTime')->name('auth.login-first-time');
    Route::get('/admin/forms/reset', 'ResetPasswordController@resetLink');
    //post
    Route::post('/admin/user/adduser', 'AdminController@store');
    Route::post('/posts/{post_id}','CommentController@store')->name('comm');
    Route::post('/admin/forms/reset', 'ResetPasswordController@ResetPasswordController')->name('resetPassword');
    //delete
    Route::delete('/comments/{$id_comment}', 'CommentController@destroy')->name('pages.delete');
});
