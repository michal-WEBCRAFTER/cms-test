<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'body', 'user_id',
    ];

    public function comments(){
        return $this->hasMany('App\Models\Comments');
    }
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
