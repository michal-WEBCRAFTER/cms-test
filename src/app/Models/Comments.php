<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    public $table = 'comments';
    protected $primaryKey = 'id_comment';
    protected $fillable = [
        'name', 'email','comment','approved','post_id'
    ];

    public  function post(){
        return $this->belongsTo('App\Models\Post');
    }
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
