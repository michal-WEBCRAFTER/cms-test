<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;


class ResetPasswordController extends Controller
{

    public function resetLink()
    {
        return view('admin.forms.reset');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function ResetPasswordController(Request $request)
    {

        $rules = array(
            'password' => 'required|string|min:6|confirmed',
        );

        $validator = Validator::make($request->all(), $rules);
        $email = $request->get('email');
        $userModel = User::where('email', $email)->first()
            ->update([
                'password' => bcrypt($request->password)
            ]);

        if ($validator->fails()) {
            return Redirect::to('admin/forms/reset')
                ->withErrors($validator)
                ->withInput();
        } else {
            if ($userModel < 3) {
                User::find($userModel->id)
                    ->create([
                        'password_changed' => $request->password_changed ? true : false,
                    ]);
                return redirect()->back()->with('message', 'Password changed');
            } else {
                return redirect()->back()->with('error', 'sth went wrong');
            }
        }
    }

}
