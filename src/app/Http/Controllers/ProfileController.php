<?php

namespace App\Http\Controllers;

use App\Models\Comments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $authAdmin = Auth::user()->admin == 0;
        if ($authAdmin)
        {
            return view('admin.forms.adduser');
        }else{
            return view('home');
        }
    }
    public function resetPassword()
    {
        return view('profile.password_reset');
    }

    public function saveNewPassword(Request $request)
    {
        $updateSth = Auth::user()->update(['password' => bcrypt($request->password)]);
        if ($updateSth) {
            return redirect()->back()->with('message', 'Password changed');
        }
        return redirect()->back()->with('error', 'sth went wrong');
    }

    public function destroy($id_comment)
    {
        // delete
        $comment = Comments::find($id_comment);
        $post_id = $comment->post->id_comment;
        $comment->delete();
        // redirect
        Session::flash('message', 'Successfully deleted the user!');
        return View::make('pages.show')
            ->with('comment', $comment);
    }

}
