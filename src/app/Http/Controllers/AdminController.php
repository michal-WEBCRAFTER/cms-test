<?php

namespace App\Http\Controllers;

use App\Mail\NewUser;
use App\Models\User;
use Session;
use View;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['dashboard','admin','edit']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('id','ASC')->select('id','name','email','created_at','updated_at')->get();
            return view('admin.dashboard', compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Mailer $mail)
    {
        $user = User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
            'admin'     => $request->admin ? false : true,
            'hash'      => str_random(32),

        ]);
        // tu wysylasz maila
        // Mail::to($user->email)->send(new UserRegistered($user));
        $mail->to($user->email)
            ->cc('k.tomczak@asper.net.pl')
            ->send(new NewUser($request->email));

        return redirect()->back()->with('message', 'User registered');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return View::make('admin.show')
            ->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        // show the edit form and pass the nerd
        return View::make('admin.edit')
            ->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'name'      => 'required|string|max:255',
            'password'  => 'required|string|min:6|confirmed',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('admin/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $user = User::find($id);
            $user->name     = $request->get('name');
            $user->email    = $request->get('email');
            $user->password = bcrypt($request->get('password'));
            $user->admin    = $request->get('admin') ? false : true;
            $user->save();

            Session::flash('message', 'Successfully updated user!');
            return Redirect::to('admin');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $user = User::find($id);
        $user->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the user!');
        return Redirect::to('admin');
    }
}
