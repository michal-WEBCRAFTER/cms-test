<?php

namespace App\Http\Controllers;

use App\Models\Comments;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use View;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $post_id)
    {
        $this->validate($request, array(
            'comment' => 'required|min:5|max:2000',
        ));

        $post = Post::find($post_id);
        $user = Auth::user()->id;
        // store
        $comment = new Comments();
        $comment->comment = $request->comment;
        $comment->approved = true;
        $comment->user()->associate($user);
        $comment->post()->associate($post);
        $comment->save();


        Session::flash('message', 'Successfully added comment!');
        return redirect()->route('comm', [$post->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    public function delete($id)
    {
        $commentes = Comments::find($id);
        return view('pages.delete', compact('commentes'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id_comment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_comment)
    {
        // delete
        $comment = Comments::find($id_comment);
        $post_id = $comment->post->id_comment;
        $comment->delete();
        // redirect
        Session::flash('message', 'Successfully deleted the user!');
        return redirect()->route('posts', [$post_id]);
    }
}


