<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class HomeController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view('home.index', compact('posts'));
    }
    public function home(){
        return view('home');

    }
    public function homeLogin(Request $request)
    {
        $auth = Auth::attempt([
            'email'     => $request->email,
            'password'  => $request->password,
        ]);
        if ($auth) {
            $user = User::where('email', $request->email)->first();
            if ($user->is_admin()) {
                return redirect()->route('home');
            }
            return redirect()->route('home');
        }
        return redirect()->back();
    }

    public function logout()
    {
        return view('home.logout');
    }
}
