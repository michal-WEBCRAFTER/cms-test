$(function () {
    $("#table").DataTable({
        responsive: true
    });

    $( "#tablecontents" ).sortable({
        items: "tr",
        cursor: 'move',
        opacity: 0.6,
        update: function() {
            sendOrderToServer();
        }
    });

    function sendOrderToServer() {

        var order = [];
        $('tr.row1').each(function(index,element) {
            order.push({
                id: $(this).attr('data-id'),
                position: index+1
            });
        });

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "{{ route('admin.dashboard') }}",
            data: {
                order:order,
                _token: '{{csrf_token()}}'
            },
            success: function(response) {
                if (response.status == "success") {
                    console.log(response);
                } else {
                    console.log(response);
                }
            }
        });

    }
});
